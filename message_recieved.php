<?php
class Message_recieved extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('admin_model/Mrecieved_msg');
		$data=$this->session->all_userdata();
		$user_id=$this->session->userdata('user_id');
		$data['rec_msg']=$this->Mrecieved_msg->recieve_msg($user_id);
		
		$this->load->view('admin/recieve_message',$data);
	}
	function index()
	{
		//redirect('message/recieve_message');
		
	}
	function delete_msg($msg_id)
	{
		
		$result=$this->Mrecieved_msg->del_message($msg_id);
		if($result==True)
		{
			$this->session->set_flashdata('msg','Message Deleted.');
			redirect('admin_controller/message','refresh');
		}
	}
	
}