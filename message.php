<?php
class Message extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('admin_model/Mmessage');
		$this->load->model('admin_model/Mrecieved_msg');
		$data=$this->session->all_userdata();
		$user_id=$this->session->userdata('user_id');
		$user_type=$this->session->userdata('user_type');
		$result['data']=$this->Mmessage->search_user();
		$result['rec_msg']=$this->Mrecieved_msg->recieve_msg($user_id);
		
		
		$this->load->view('admin/message',$result);
	}
	function index()
	{
		//redirect('message/recieve_message');
		
	}
	function send_message()
	{
		if($this->session->userdata('user_id'))
		{
			
			$user_id=$this->session->userdata('user_id');
			$friend_id=$this->input->post('friend_id');
			$message=$this->input->post('my_message');
			$user_type=$this->session->userdata('user_type');
			if($user_type=="user")
			{
				$result['bad_words']=$this->Mmessage->validate_msg(); 
				//print_r($result['bad_words']);
				$disallowed=array('shucks','test');
				$message= word_censor($message,$disallowed,'##');
				
			}
			
			
			
			$data = array( 
		   'sender_id' => "$user_id", 
		   'reciever_id' => "$friend_id",
		   'content'=> "$message",
		   
			); 
			$this->Mmessage->add_msg($data); 
			$this->session->set_userdata('msg_send','Message Send.');
			redirect('admin_controller/message','refresh');
			//$this->load->view('message','refresh');
			
		}
	}
	function recieve_message()
	{
		
		if($this->session->userdata('user_id'))
		{
			$user_id=$this->session->userdata('user_id');
			$result['response']=$this->Mmessage->recieve_msg($user_id); 
			//$this->session->set_userdata('msg','Message Send.');
			$this->load->view('admin/message',$result);
		}
		
	}
	
}