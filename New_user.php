<?php
class New_user extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		
		//$this->load->library('session');
		$this->load->helper('url');
		$this->load->model('admin_model/mnew_user','mnew_user');
		
		
	}
	function index()
	{
		$result['userlist']=$this->mnew_user->get_user();
		$this->load->view('admin/vnew_user',$result);
	}
	function delete_user($user_id)
	{
		
		$result=$this->mnew_user->del_user($user_id);
		if($result==True)
		{
			$this->session->set_flashdata('msg_del','Member Record Deleted.');
			redirect('admin_controller/new_user','refresh');
		}
	}
	function add_user_form()
	{
		$this->load->view('admin/vadd_user_form');
		
	}
	
	function add_user()
	{
		$uname=$this->input->post('uname');
		$exist=$this->mnew_user->check_username($uname);
		
		if($exist==True)
		{
		
			if($this->session->userdata('user_id'))
			{
				
				$user_id=$this->session->userdata('user_id');
				$role=$this->input->post('role');
				$fname=$this->input->post('fname');
				$lname=$this->input->post('lname');
				$uname=$this->input->post('uname');
				$pwd=$this->input->post('pwd');
				$email=$this->input->post('email');
				$mobile=$this->input->post('mobile');
				$gender=$this->input->post('gender');
				$data = array( 
			   'user_type' => "$role", 
			   'first_name' => "$fname",
			   'last_name'=> "$lname",
			   'username'=> "$uname",
			   'password'=> "$pwd",
			   'email'=> "$email",
			   'mobile_no'=> "$mobile",
			   'gender'=> "$gender",
			   'registered_by'=> "$user_id"
			  
				);

				if($this->mnew_user->add_new_user($data))
				{
					$this->session->set_flashdata('msg','New Member Added!!');
					$this->load->view('admin/vadd_user_form');
				}
					
				
				
			}
		}
		else
		{
			$this->session->set_flashdata('msg','Username Already Exist..');
			$this->load->view('admin/vadd_user_form');
		}
	}
	
}