<?php
class Dashboard extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		
		//$this->load->library('session');
		$this->load->helper('url');
		$this->load->model('admin_model/Mdashboard','Mdashboard');
		$user_id=$this->session->userdata("user_id");
		
		$result['pic']=$this->Mdashboard->get_img();
				
				 $result['profile_img']=$result['pic'][0];
				 
				
		
		if($this->session->userdata('user_id')=="")
		{
			redirect('login','refresh');
			//$this->load->view('dashboard');
		}
		else
		{
			
			$result['data']=$this->Mdashboard->get_message($user_id);
			
		}
		$this->load->view('admin/dashboard',$result);
				 
	}
	function index()
	{
		
		
		
	}
	function change_pic()
	{
		$this->session->set_userdata('change_pic','yes');
		$change_status=$this->session->userdata('change_pic');
		redirect('admin_controller/dashboard','refresh');
	}
	function store_img()
	{
		
		//echo $image=$this->input->post('userfile');
			//$new_path="./images/".$image;
		 $config['upload_path']   = './images'; 
         $config['allowed_types'] = 'gif|jpg|png'; 
         $config['max_size']      = 1000000; 
         $config['max_width']     = 1024; 
         $config['max_height']    = 76800;  
		  $this->load->library('upload', $config);
		 if (!$this->upload->do_upload('userfile')) {
           $error = array('error' => $this->upload->display_errors());
			print_r($error);
            //$this->load->view('files/upload_form', $error);
         }
			
         else { 
		 
            $data = array('upload_data' => $this->upload->data()); 
            //$this->load->view('dashboard', $data); 
			$upload_data = $this->upload->data(); //Returns array of containing all of the data related to the file you uploaded.
			 $file_name = $upload_data['file_name'];
			$path="images/".$file_name;
			 
			 $data = array( 
			   
			   'profile_pic' => "$path" 
			); 
		
				
			 if($this->Mdashboard->upload_image($data)>0)
			 {
				
				 $result['pic']=$this->Mdashboard->get_img();
				 //print_r($result);	
				
				 $data['profile_img']=$result['pic'][0];
				// print_r($data['profile_img'][0]);
				  //echo "hii";
				// $data['pic']=$path;
				 //$this->load->view('admin/dashboard',$data);
				 redirect('dashboard','refresh');
			 }
			 else
			 {
				// echo "not";
			 }
         } 
	}
	
	function delete_msg($msg_id)
	{
		$this->load->model('admin_model/Mdashboard');
		$result=$this->Mdashboard->del_message($msg_id);
		if($result==True)
		{
			redirect('admin_controller/dashboard','refresh');
		}
	}
}