<?php
class Blacklist extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		
		//$this->load->library('session');
		$this->load->helper('url');
		$this->load->model('admin_model/mbadword','mbadword');
		
		
	}
	function index()
	{
		$result['badwordlist']=$this->mbadword->get_badword();
		$this->load->view('admin/vbadword',$result);
	}
	function delete_word($word_id)
	{
		
		$result=$this->mbadword->del_word($word_id);
		if($result==True)
		{
			$this->session->set_flashdata('msg_delword','Word Remove from Blacklist..');
			redirect('admin_controller/blacklist','refresh');
		}
	}
	function add_badword_form()
	{
		$this->load->view('admin/vadd_badword_form');
		
	}
	
	function add_badword()
	{
		$word=$this->input->post('word');
		$exist=$this->mbadword->check_word($word);
		
		if($exist==True)
		{
		
			if($this->session->userdata('user_id'))
			{
				
				$user_id=$this->session->userdata('user_id');
				
				
				$data = array( 
			   'bad_word' => "$word"
			  
			  
				);

				if($this->mbadword->add_bad_word($data))
				{
					$this->session->set_flashdata('msg_word','New Word Block!!');
					$this->load->view('admin/vadd_badword_form');
				}
					
				
				
			}
		}
		else
		{
			$this->session->set_flashdata('msg_word','Already Exist in Blacklist..');
			$this->load->view('admin/vadd_badword_form');
		}
	}
	
}